func: fib-tail (0 Int Int) (rotr drop2)
func: fib-tail (1 Int Int) (rotl drop2)
func: fib-tail (Int Int Int) (rotr -- rotl dup rotl + recall: fib-tail)

10 bench: (10000 times:, 20 0 1 fib-tail drop) say