from bench import bench

print(bench(10, '', '''
s = []

for _ in range(100000):
  s.append([1, 2])
  s.append(3)
  s.pop()

for _ in range(100000):
  s.pop()
'''))

