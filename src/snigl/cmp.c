#include "snigl/cmp.h"

enum sgl_cmp sgl_char_cmp(char lhs, char rhs) {
  if (lhs < rhs) { return SGL_LT; }
  return (lhs > rhs) ? SGL_GT : SGL_EQ;
}

enum sgl_cmp sgl_ptr_cmp(const void *lhs, const void *rhs) {
  if (lhs < rhs) { return SGL_LT; }
  return (lhs > rhs) ? SGL_GT : SGL_EQ;
}
