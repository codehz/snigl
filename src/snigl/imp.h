#ifndef SNIGL_IMP_H
#define SNIGL_IMP_H

struct sgl_form;
struct sgl_ls;

struct sgl_imp {
  struct sgl_form *form;
  struct sgl_op *start_pc, *end_pc;
  bool recalls;
};

struct sgl_imp *sgl_imp_init(struct sgl_imp *imp, struct sgl_form *form);
struct sgl_imp *sgl_imp_deinit(struct sgl_imp *imp, struct sgl *sgl);

#endif
