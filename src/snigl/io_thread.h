#ifndef SNIGL_IO_THREAD_H
#define SNIGL_IO_THREAD_H

#include <pthread.h>
#include "snigl/ls.h"

struct sgl;
struct sgl_io;

struct sgl_io_thread {
  struct sgl *sgl;
  pthread_t imp;
  struct sgl_ls ls, queue;
  pthread_mutex_t queue_lock;
  pthread_cond_t queue_cond;
  bool stopped;
};

struct sgl_io_thread *sgl_io_thread_new(struct sgl *sgl);
void sgl_io_thread_free(struct sgl_io_thread *t, struct sgl *sgl);
bool sgl_io_thread_start(struct sgl_io_thread *t);
bool sgl_io_thread_stop(struct sgl_io_thread *t);
bool sgl_io_thread_push(struct sgl_io_thread *t, struct sgl_io *io);

#endif
