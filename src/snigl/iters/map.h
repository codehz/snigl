#ifndef SNIGL_ITERS_MAP_H
#define SNIGL_ITERS_MAP_H

#include "snigl/iter.h"

struct sgl_map_iter {
  struct sgl_iter iter;
  struct sgl_iter *in;
  struct sgl_val *fn;
};

struct sgl_map_iter *sgl_map_iter_new(struct sgl *sgl,
                                      struct sgl_iter *in,
                                      struct sgl_val *fn);

#endif
