#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "snigl/pool.h"
#include "snigl/scope.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/var.h"

struct sgl_scope *sgl_scope_new(struct sgl *sgl, struct sgl_scope *parent) {
  return sgl_scope_init(sgl_malloc(&sgl->scope_pool), parent);
}

struct sgl_scope *sgl_scope_init(struct sgl_scope *s, struct sgl_scope *parent) {
  s->parent = parent;
  if (parent) { parent->nrefs++; }
  sgl_lset_init(&s->vars, sgl_var_cmp);
  s->nrefs = 1;
  return s;
}

struct sgl_scope *sgl_scope_deinit(struct sgl_scope *s, struct sgl *sgl) {
  if (s->parent) { sgl_scope_deref(s->parent, sgl); }
  sgl_ls_do(&s->vars.root, struct sgl_var, ls, v) { sgl_var_free(v, sgl); }
  return s;
}

void sgl_scope_free(struct sgl_scope *s, struct sgl *sgl) {
  sgl_free(&sgl->scope_pool, sgl_scope_deinit(s, sgl));
}

void sgl_scope_deref(struct sgl_scope *s, struct sgl *sgl) {
  assert(s->nrefs > 0);
  if (!--s->nrefs) { sgl_scope_free(s, sgl); }
}
