#include "snigl/call.h"
#include "snigl/list.h"
#include "snigl/sgl.h"
#include "snigl/task.h"
#include "snigl/try.h"
#include "snigl/val.h"

struct sgl_task *sgl_task_init(struct sgl_task *t,
                               struct sgl *sgl,
                               struct sgl_lib *lib,
                               struct sgl_scope *parent_scope,
                               struct sgl_op *start_pc,
                               struct sgl_op *end_pc) {
  sgl_ls_init(&t->calls);
  sgl_ls_init(&t->reg_stack);
  sgl_ls_init(&t->scopes);
  sgl_ls_init(&t->stacks);
  
  sgl_vec_init(&t->libs, sizeof(struct sgl_lib *));
  t->lib = *(struct sgl_lib **)sgl_vec_push(&t->libs) = lib;

  struct sgl_list *ms = &t->main_stack;
  sgl_list_init(ms);
  sgl_ls_push(&t->stacks, &ms->ls);
  t->stack = ms;
  ms->nrefs++;
  
  sgl_ls_init(&t->tries);
  sgl_ls_push(&sgl->tasks, &t->ls);

  if (parent_scope) {
    sgl_ls_push(&t->scopes, &sgl_scope_new(sgl, parent_scope)->ls);
  }
    
  t->pc = start_pc;
  t->end_pc = end_pc;
  sgl->ntasks++;
  return t;
}

struct sgl_task *sgl_task_deinit(struct sgl_task *t, struct sgl *sgl) {
  sgl_vec_deinit(&t->libs);
  sgl_ls_do(&t->tries, struct sgl_try, ls, _t) { sgl_try_free(_t, sgl); }
  sgl_ls_do(&t->scopes, struct sgl_scope, ls, s) { sgl_scope_deref(s, sgl); }
  sgl_ls_do(&t->calls, struct sgl_call, ls, c) { sgl_call_free(c, sgl); }
  sgl_ls_do(&t->reg_stack, struct sgl_val, ls, v) { sgl_val_free(v, sgl); }
  sgl_ls_do(&t->stacks, struct sgl_list, ls, s) { sgl_list_deref(s, sgl); }
  sgl_list_deinit(&t->main_stack, sgl);
  return t;
}

struct sgl_task *sgl_task_new(struct sgl *sgl,
                              struct sgl_lib *lib,
                              struct sgl_scope *parent_scope,
                              struct sgl_op *start_pc,
                              struct sgl_op *end_pc) {
  return sgl_task_init(sgl_malloc(&sgl->task_pool),
                       sgl, lib, parent_scope, start_pc, end_pc);
}

void sgl_task_free(struct sgl_task *t, struct sgl *sgl) {
  sgl_free(&sgl->task_pool, sgl_task_deinit(t, sgl));
}
