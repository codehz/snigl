#ifndef SNIGL_TYPE_H
#define SNIGL_TYPE_H

#include "snigl/cmp.h"
#include "snigl/config.h"
#include "snigl/def.h"
#include "snigl/vset.h"

#define sgl_types(...)                          \
  (struct sgl_type *[]) { __VA_ARGS__, NULL }   \

#define sgl_type_union(sgl, pos, ...) \
  _sgl_type_union(sgl, pos, sgl_types(__VA_ARGS__))

struct sgl_buf;
struct sgl_cemit;
struct sgl_iter;
struct sgl_val;

struct sgl_type {
  struct sgl_def def;
  struct sgl_ls ls;
  struct sgl_type *meta;
  bool is_meta, is_ref, is_trait;
  sgl_int_t cemit_id, parent_offs, tag;
  struct sgl_type *parents[SGL_MAX_TYPES];
  struct sgl_vset children;
 
  bool (*bool_val)(struct sgl_val *);

  struct sgl_op *(*call_val)(struct sgl_val *,
                             struct sgl *,
                             struct sgl_pos *,
                             struct sgl_op *,
                             bool);
  
  bool (*cemit_val)(struct sgl_val *,
                    struct sgl *,
                    struct sgl_pos *,
                    const char *,
                    const char *,
                    struct sgl_cemit *);

  bool (*clone_val)(struct sgl_val *,
                    struct sgl *,
                    struct sgl_pos *,
                    struct sgl_val *);
  
  enum sgl_cmp (*cmp_val)(struct sgl_val *, struct sgl_val *);
  void (*deinit_val)(struct sgl_val *, struct sgl *);
  void (*dump_val)(struct sgl_val *, struct sgl_buf *);
  void (*dup_val)(struct sgl_val *, struct sgl *, struct sgl_val *);
  bool (*eq_val)(struct sgl_val *, struct sgl *, struct sgl_val *);
  bool (*is_val)(struct sgl_val *, struct sgl_val *);
  struct sgl_iter *(*iter_val)(struct sgl_val *, struct sgl *, struct sgl_type **);

  void (*print_val)(struct sgl_val *,
                    struct sgl *,
                    struct sgl_pos *,
                    struct sgl_buf *);

  void (*free)(struct sgl_type *, struct sgl *);
};

extern struct sgl_def_type SGL_DEF_TYPE;

struct sgl_type *sgl_type_init(struct sgl_type *t,
                               struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_lib *lib,
                               struct sgl_sym *id,
                               struct sgl_type *parents[]);

struct sgl_type *sgl_type_new(struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_lib *lib,
                              struct sgl_sym *id,
                              struct sgl_type *parents[]);

struct sgl_type *sgl_trait_new(struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_lib *lib,
                               struct sgl_sym *id,
                               struct sgl_type *parents[]);

void sgl_type_free(struct sgl_type *t, struct sgl *sgl);
struct sgl_sym *sgl_type_id(struct sgl_type *t);

sgl_int_t sgl_type_cemit_id(struct sgl_type *t,
                            struct sgl *sgl,
                            struct sgl_pos *pos,
                            struct sgl_cemit *out);

struct sgl_type *sgl_type_meta(struct sgl_type *t, struct sgl *sgl);
void sgl_derive(struct sgl_type *type, struct sgl_type *parent);
bool sgl_derived(struct sgl_type *type, struct sgl_type *parent);

struct sgl_type *_sgl_type_union(struct sgl *sgl,
                                 struct sgl_pos *pos,
                                 struct sgl_type *types[]);

struct sgl_type *sgl_type_union_new(struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_lib *lib,
                                    struct sgl_sym *id,
                                    struct sgl_type *types[]);

void sgl_setup_types();

#endif
